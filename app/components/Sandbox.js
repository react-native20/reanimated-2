import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function Sandbox(props) {
  return (
    <View style={styles.container}>
      <Text>Reanimated 2 Sandbox</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
